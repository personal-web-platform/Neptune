CREATE TABLE neptune.technologies
(
    id       SERIAL PRIMARY KEY,
    name     VARCHAR(255) UNIQUE NOT NULL,
    status   VARCHAR(255)        NOT NULL,
    category VARCHAR(255)        NOT NULL,
    link     VARCHAR(255)        NOT NULL,
    icon     VARCHAR(255)        NOT NULL
);
