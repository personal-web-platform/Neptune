package eu.francoisdevilez.managers

import eu.francoisdevilez.models.dtos.RepositoryDTO
import eu.francoisdevilez.models.repositories.RepositoryRepository
import jakarta.inject.Singleton
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

@Singleton
class RepositoryManager(
    private val repositoryRepository: RepositoryRepository
) {
    fun getRepositories(): Flow<RepositoryDTO> = this.repositoryRepository.findAll().map { it.toDto() }
}