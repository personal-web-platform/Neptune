package eu.francoisdevilez.managers

import eu.francoisdevilez.models.dtos.LogDTO
import eu.francoisdevilez.models.dtos.RequestMethod
import eu.francoisdevilez.models.dtos.TechnologyDTO
import eu.francoisdevilez.models.entities.Technology
import eu.francoisdevilez.models.repositories.TechnologyRepository
import eu.francoisdevilez.utils.Logger
import jakarta.inject.Singleton
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.util.*

@Singleton
class TechnologyManager(
    private val logger: Logger,
    private val technologyRepository: TechnologyRepository
) {
    fun getTechnologies(): Flow<TechnologyDTO> = this.technologyRepository.findAll().map { it.toDto() }

    suspend fun saveOrUpdateTechnology(technologyDTO: TechnologyDTO): TechnologyDTO {
        this.technologyRepository.findByName(technologyDTO.name)?.let {
            return this.updateTechnology(it, technologyDTO)
        } ?: run {
            return this.saveTechnology(
                Technology(
                    technologyDTO.name,
                    technologyDTO.status,
                    technologyDTO.category.orEmpty(),
                    technologyDTO.link,
                    technologyDTO.icon
                )
            )
        }
    }

    private suspend fun updateTechnology(technology: Technology, technologyDTO: TechnologyDTO): TechnologyDTO {
        logger.sendLog(
            LogDTO.createLog(
                "Updating technology $technology to $technologyDTO",
                "/technologies",
                RequestMethod.POST
            )
        )
        technology.status = technologyDTO.status
        return this.technologyRepository.update(technology).toDto()
    }

    private suspend fun saveTechnology(technology: Technology): TechnologyDTO {
        logger.sendLog(
            LogDTO.createLog(
                "Creating new technology $technology",
                "/technologies",
                RequestMethod.POST
            )
        )
        return this.technologyRepository.save(technology).toDto()
    }

    suspend fun deleteTechnology(technologyName: Map<String, String>): Int {
        this.technologyRepository.findByName(technologyName["name"]!!.lowercase(Locale.getDefault())).let {
            if (it == null) {
                return 0
            }
            logger.sendLog(
                LogDTO.createLog(
                    "Deleting new technology $technologyName",
                    "/technologies",
                    RequestMethod.POST
                )
            )
            return this.technologyRepository.delete(it)
        }
    }
}