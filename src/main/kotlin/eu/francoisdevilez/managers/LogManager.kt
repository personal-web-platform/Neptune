package eu.francoisdevilez.managers

import eu.francoisdevilez.models.dtos.LogsDTO
import eu.francoisdevilez.models.dtos.StatisticDTO
import io.micronaut.http.HttpHeaders.ACCEPT
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.uri.UriBuilder
import jakarta.inject.Singleton
import kotlinx.coroutines.reactive.awaitSingle
import java.time.OffsetDateTime

@Singleton
class LogManager(
    @param:Client(id = "logger") private val httpClient: HttpClient
) {
    suspend fun getLogs(page: Int, size: Int): LogsDTO =
        httpClient.retrieve(
            HttpRequest
                .GET<Any>(
                    UriBuilder
                        .of("/log")
                        .queryParam("page", page)
                        .queryParam("size", size)
                        .build()
                )
                .header(ACCEPT, "application/json"),
            LogsDTO::class.java
        ).awaitSingle()

    suspend fun getStatistics(start: OffsetDateTime, end: OffsetDateTime): StatisticDTO =
        httpClient.retrieve(
            HttpRequest
                .GET<Any>(
                    UriBuilder
                        .of("/log/statistics")
                        .queryParam("start", start)
                        .queryParam("end", end)
                        .build()
                )
                .header(ACCEPT, "application/json"),
            StatisticDTO::class.java
        ).awaitSingle()
}