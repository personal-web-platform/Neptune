/*
 *
 *  * Copyright (C) 2022 FRANCOIS DEVILEZ
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package eu.francoisdevilez.utils

import eu.francoisdevilez.models.dtos.LogDTO
import eu.francoisdevilez.models.dtos.LogLevels
import io.micronaut.context.annotation.Value
import io.micronaut.http.HttpHeaders.ACCEPT
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.uri.UriBuilder
import jakarta.inject.Singleton
import kotlinx.coroutines.reactive.awaitSingle
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Singleton
class Logger(
    @param:Client(id = "logger") private val httpClient: HttpClient,
    @Value("\${neptune.dev}") private val dev: Boolean
) {
    private val logger: Logger = LoggerFactory.getLogger(Logger::class.java)

    suspend fun sendLog(log: LogDTO): LogDTO {
        this.logMessage(log)
        if (!dev) return this.sendLogToSun(log)
        return log
    }

    private fun logMessage(log: LogDTO) {
        when (log.logLevel) {
            LogLevels.INFO -> logger.info(log.message)
            LogLevels.ERROR -> logger.error(log.message)
        }
    }

    private suspend fun sendLogToSun(log: LogDTO): LogDTO {
        val req: HttpRequest<LogDTO> = HttpRequest
            .POST(UriBuilder.of(LOG_PATH).build(), log)
            .header(ACCEPT, "application/json")
        return httpClient.retrieve(req, LogDTO::class.java).awaitSingle()
    }

    companion object {
        const val LOG_PATH = "/log"
    }
}