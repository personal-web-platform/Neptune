package eu.francoisdevilez.models.repositories

import eu.francoisdevilez.models.entities.Technology
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.r2dbc.annotation.R2dbcRepository
import io.micronaut.data.repository.kotlin.CoroutineCrudRepository

@R2dbcRepository(dialect = Dialect.POSTGRES, dataSource = "neptune")
interface TechnologyRepository : CoroutineCrudRepository<Technology, Long> {
    suspend fun findByName(name: String): Technology?
}