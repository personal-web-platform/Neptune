package eu.francoisdevilez.models.dtos

import io.micronaut.serde.annotation.Serdeable

@Serdeable
data class LogsDTO(
    val totalNumberOfLogs: Int,
    val logs: List<LogDTO>
)
