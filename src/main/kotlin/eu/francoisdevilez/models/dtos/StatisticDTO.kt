package eu.francoisdevilez.models.dtos

import io.micronaut.serde.annotation.Serdeable

@Serdeable
data class StatisticDTO(
    val numberOfLogs: Int,
    val numberOfProjects: Int,
    val numberOfLogsPerProject: List<LogProject>,
    val numberOfLogsPerDay: List<LogDay>,
    val averageNumberOfLogsPerProject: List<AvgLogProject>
)

@Serdeable
data class LogProject(val total: Int, val projectName: String)

@Serdeable
data class LogDay(val total: Int, val date: String)

@Serdeable
data class AvgLogProject(val average: Int, val projectName: String)