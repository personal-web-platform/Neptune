package eu.francoisdevilez.models.dtos

import io.micronaut.serde.annotation.Serdeable
import java.util.*

@Serdeable
data class RepositoryDTO(
    val groupName: String,
    val name: String,
    val description: String,
    val createdAt: Date,
    val lastActivityAt: Date,
    val webUrl: String,
    val visibility: String,
    var latestTag: String,
    val language: String
)
