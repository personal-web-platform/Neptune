package eu.francoisdevilez.models.dtos

import io.micronaut.serde.annotation.Serdeable

@Serdeable
data class TechnologyDTO(
    val id: Long?,
    val name: String,
    val status: String,
    val category: String?,
    val link: String,
    val icon: String
)
