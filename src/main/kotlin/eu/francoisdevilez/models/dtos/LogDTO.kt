/*
 *
 *  * Copyright (C) 2022 FRANCOIS DEVILEZ
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package eu.francoisdevilez.models.dtos

import io.micronaut.serde.annotation.Serdeable
import java.time.OffsetDateTime

enum class LogLevels {
    INFO, ERROR;
}

enum class RequestMethod {
    NONE, POST, GET
}

@Serdeable
data class LogDTO(
    val id: Long? = null,
    val message: String,
    val endpoint: String = "-",
    val logLevel: LogLevels = LogLevels.INFO,
    val requestMethod: RequestMethod = RequestMethod.NONE,
    val createdAt: OffsetDateTime? = null,
    val updatedAt: OffsetDateTime? = null,
    val project: String = "mercury"
) {
    companion object {
        fun createLog(message: String, endpoint: String, requestMethod: RequestMethod): LogDTO {
            return LogDTO(message = message, endpoint = endpoint, requestMethod = requestMethod)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LogDTO

        if (id != other.id) return false
        if (message != other.message) return false
        if (endpoint != other.endpoint) return false
        if (logLevel != other.logLevel) return false
        if (requestMethod != other.requestMethod) return false
        if (createdAt != other.createdAt) return false
        if (updatedAt != other.updatedAt) return false
        if (project != other.project) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + message.hashCode()
        result = 31 * result + endpoint.hashCode()
        result = 31 * result + logLevel.hashCode()
        result = 31 * result + requestMethod.hashCode()
        result = 31 * result + (createdAt?.hashCode() ?: 0)
        result = 31 * result + (updatedAt?.hashCode() ?: 0)
        result = 31 * result + project.hashCode()
        return result
    }

    override fun toString(): String {
        return "LogDTO(id=$id, message='$message', endpoint='$endpoint', logLevel=$logLevel, requestMethod=$requestMethod, createdAt=$createdAt, updatedAt=$updatedAt, project='$project')"
    }
}
