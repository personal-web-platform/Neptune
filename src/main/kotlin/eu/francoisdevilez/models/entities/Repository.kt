package eu.francoisdevilez.models.entities

import eu.francoisdevilez.models.dtos.RepositoryDTO
import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity
import java.util.*

@MappedEntity(schema = "mercury", value = "repositories")
data class Repository(
    val groupName: String,
    val name: String,
    val description: String,
    val createdAt: Date,
    val lastActivityAt: Date,
    val webUrl: String,
    val visibility: String,
    var latestTag: String,
    val language: String
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    fun toDto(): RepositoryDTO {
        return RepositoryDTO(groupName, name, description, createdAt, lastActivityAt, webUrl, visibility, latestTag, language)
    }
}
