package eu.francoisdevilez.models.entities

import eu.francoisdevilez.models.dtos.TechnologyDTO
import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity

@MappedEntity(schema = "neptune", value = "technologies")
data class Technology(
    val name: String,
    var status: String,
    val category: String,
    val link: String,
    val icon: String
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    fun toDto(): TechnologyDTO {
        return TechnologyDTO(id, name, status, category, link, icon)
    }
}
