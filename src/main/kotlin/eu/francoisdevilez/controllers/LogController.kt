package eu.francoisdevilez.controllers

import eu.francoisdevilez.managers.LogManager
import eu.francoisdevilez.models.dtos.LogsDTO
import eu.francoisdevilez.models.dtos.StatisticDTO
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue
import java.time.OffsetDateTime

@Controller(value = "/logs")
class LogController(
    private val logManager: LogManager
) {
    @Get(produces = [MediaType.APPLICATION_JSON])
    suspend fun getLogs(@QueryValue pageNumber: Int, @QueryValue pageSize: Int): HttpResponse<LogsDTO> =
        HttpResponse.ok(logManager.getLogs(pageNumber, pageSize))

    @Get(value = "/statistics", produces = [MediaType.APPLICATION_JSON])
    suspend fun getStatistics(
        @QueryValue start: OffsetDateTime,
        @QueryValue end: OffsetDateTime
    ): HttpResponse<StatisticDTO> = HttpResponse.ok(logManager.getStatistics(start, end))
}