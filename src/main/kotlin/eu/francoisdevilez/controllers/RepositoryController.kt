package eu.francoisdevilez.controllers

import eu.francoisdevilez.managers.RepositoryManager
import eu.francoisdevilez.models.dtos.RepositoryDTO
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import kotlinx.coroutines.flow.Flow

@Controller(value = "/repositories")
class RepositoryController(
    private val repositoryManager: RepositoryManager
) {
    @Get(produces = [MediaType.APPLICATION_JSON])
    fun getRepositories(): HttpResponse<Flow<RepositoryDTO>> = HttpResponse.ok(this.repositoryManager.getRepositories())
}