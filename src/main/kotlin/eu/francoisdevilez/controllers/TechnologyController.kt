package eu.francoisdevilez.controllers

import eu.francoisdevilez.managers.TechnologyManager
import eu.francoisdevilez.models.dtos.TechnologyDTO
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import jakarta.validation.Valid
import kotlinx.coroutines.flow.Flow

@Controller(value = "/technologies")
class TechnologyControllerController(
    private val technologyManager: TechnologyManager
) {
    @Get(produces = [MediaType.APPLICATION_JSON])
    fun getTechnologies(): HttpResponse<Flow<TechnologyDTO>> =
        HttpResponse.ok(this.technologyManager.getTechnologies())

    @Post(produces = [MediaType.APPLICATION_JSON], consumes = [MediaType.APPLICATION_JSON])
    suspend fun saveOrUpdateTechnology(@Body @Valid technologyDTO: TechnologyDTO): HttpResponse<TechnologyDTO> =
        HttpResponse.created(this.technologyManager.saveOrUpdateTechnology(technologyDTO))

    @Delete(produces = [MediaType.TEXT_PLAIN], consumes = [MediaType.APPLICATION_JSON])
    suspend fun deleteTechnology(@Body body: Map<String, String>): HttpResponse<Int> =
        HttpResponse.ok(this.technologyManager.deleteTechnology(body))
}